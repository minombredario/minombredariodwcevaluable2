$('document').ready(function () {
    CargarTienda();
});

var articulos = [];


function CargarTienda() {

	articulos = [];

    var url = "http://hispabyte.net/DWEC/entregable2-3.php";;

    $.ajax({

        url: url,

        method: 'GET',

    }).done(function(result) {

        result = JSON.parse(result);

        for (var i=0; i<result.length; i++) {

            articulos.push([parseInt(result[i].id), result[i].nombre, parseInt(result[i].cat), parseInt(result[i].precio), parseInt(result[i].unidades)]);//pasamos los numeros a int para poder tratarlos como numeros

        }

        articulos.sort(function(a,b){ //ordenar articulos por categoria ascendente

            return b[2]-a[2];

        });

		ListarArticulos();

    }).fail(function(err) {//tratamiento de errores

        throw err;
    });
}

function ListarArticulos(){

	if (localStorage.getItem('articulos')) {

        $('#articulos').append(localStorage.getItem('articulos'));

    } else if (localStorage.getItem('Original')){

        $('#articulos').append(localStorage.getItem('Original'));

    } else {

       for (var i=0; i<articulos.length; i++) {

             $('#articulos').append(/*'<tr><td>' + articulos[i][0] + */
									'<tr><td>' + articulos[i][2] +
									'</td><td>' + articulos[i][1] +
									'</td><td>' + articulos[i][3] +
									'€</td><td id="stock-' + i + '">' + articulos[i][4] +
									'</td><td><input type="text" id="cnt' + i + '" placeholder="cantidad"/>' +
									'  <input class="btn btn-primary" id="boton-' + i + '" value="Añadir al carrito" data-id=' + i +' /></td></tr>');

        } //En este punto guardamos la configuracion original de la tienda

        localStorage.setItem('Original',$('#articulos').html());

    }



    //recuperamos lo que hubiera en el carrito

    $('#carrito').append(localStorage.getItem("carrito"));


    $('input[data-id]').each(function() {

         $(this).click(function(){

           var dataId = $(this).attr('data-id');

           var total = $('#cnt'+dataId).val();

           anyadirCarrito(total,dataId);

            $('#cnt'+dataId).val(" ");

        });

    });

    //borrar articulos del carrito //

    $('#borrar').on('click', function(){

        $('input[type=text]').val(" ");

        $('input[type=button]').unbind(); // unbind elimina los eventos generados para no repetirlos

        $('#carrito').empty();

        $('#articulos').empty();

        localStorage.removeItem('carrito');

        localStorage.removeItem('articulos');

        ListarArticulos	();

        /*en esta parte hay que hacer una llamada recursiva a la Funcion ListarArticulos para
		restaurar la cantidad de cada articulos que se ha liberado*/
    });

	// recuperar los datos del servidor y eliminar los datos almacenados en el localStorage

    $('#recuperar').on('click', function(){

		$('input[type=button]').unbind();

        $('#articulos').empty();

        $('#carrito').empty();

        localStorage.removeItem("carrito");

        localStorage.removeItem("Original");

        localStorage.removeItem("articulos");

        CargarTienda();

    });

}

function anyadirCarrito(cantidadArticulos,dataId) {

    if($.isNumeric(cantidadArticulos)) {

       var TotalStock = "#stock-" + dataId;

       var diferencia = parseInt($(TotalStock).html()) - parseInt(cantidadArticulos);

       if (diferencia >= 0) {

           var total = cantidadArticulos*articulos[dataId][3];

           $(TotalStock).html(diferencia);

           $('#carrito').append(//'<tr><td>' + articulos[dataId][0] +
								'<tr><td>' + articulos[dataId][2] +
								'</td><td> ' + articulos[dataId][1] +
								'</td><td>' +  articulos[dataId][3] +
								'€</td><td>' + cantidadArticulos +
								'</td><td>' +  total +
								'</td></tr>');

            localStorage.setItem("carrito",$("#carrito").html()); // Estado del carrito

            localStorage.setItem("articulos",$("#articulos").html()); //Estado de la tienda

       } else {

           alert("No hay suficientes unidades en el Almacen");
       }
    } else {

           alert("El valor introducido no es numérico");
       }
}

