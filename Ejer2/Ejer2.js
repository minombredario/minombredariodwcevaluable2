$('document').ready(function () {
    $('#enviar').click(function (){
        busqueda();
    });

    $('#ordenar').change(ListarEquipos);
});


var equipos = [];

var puntos = [];

var goles = [];

function busqueda() {

    equipos = [];


    var url = "http://hispabyte.net/DWEC/entregable2-2.php";

    $.ajax({

        url: url,

        method: 'GET',

    }).done(function(result) {

        result = JSON.parse(result);

        for (var i=0; i<result.length; i++) {

            equipos.push([result[i].nombre,parseInt(result[i].puntos),parseInt(result[i].golesAFavor)]);//pasamos los numeros a int para poder ordenarlos como numeros

        }

        ListarEquipos();

    }).fail(function(err) {//tratamiento de errores

        throw err;
    });
}

function ListarEquipos(){

	var ordenados = [];

    $("#datos_entrada").empty();

    switch ($("#ordenar option:checked").val()) { //reordenación según interés
        case "original":
            ordenados = equipos; //original
            break;
        case "puntos":
            ordenados = ordenar(equipos, "puntos");
            break;
        case "goles":
            ordenados = ordenar(equipos, "goles");
            break;
    }
    $('#datos_entrada').append('<table class="table table-hover tabla"><thead style="background-color: #337ab7"><tr><th>Equipo</th><th>Puntos</th><th>Goles</th></tr></thead><tr>');

    for (var i=0; i<ordenados.length; i++) {
        $("#datos_entrada .tabla").append("<tr><td>" + ordenados[i][0] + "</td><td>" + ordenados[i][1] + "</td><td>" + ordenados[i][2] + "</td></tr>");
    }
	$('#datos_entrada').append('</tr></table>');
}



function ordenar(matriz, orden) {

    if (orden==="puntos") { //ordenar por puntos guardados en la posición 1 del vector
        matriz.sort(function (a, b) {
            return b[1]-a[1];
        });
    } else if(orden==="goles") { //ordenar por goles guardados en la posición 2 del vector
        matriz.sort(function (a, b) {
            return b[2]-a[2];
        });
    }
    return matriz;
}



