$(document).ready(function(){

	$('#F_inicio').blur(ValidarFecha);

	$('#F_fin').blur(ValidarFecha);

	$("#enviar").click(function (){
		if($('#busqueda').val() != "" && $('#F_inicio').val() != "" && $('#F_fin').val() != ""){

			buscar($('#busqueda').val(),$('#F_inicio').val(),$('#F_fin').val());

		}else{

			alert("rellena todos los campos");

			$('#busqueda').focus();
		}


    });

    $("#ordenar").change(Noticias);

});

/* Funciones para comprobar el formato de las fechas, que tengan 8 digitos y la primera sea mayor que la segunda*/

function ValidarFecha(){

	$('#F_fin').css('color','black');

	$('.errorI').text('');

	$('.errorF').text('');

	var inicio = $('#F_inicio').val();

	var fin = $('#F_fin').val();

	if(isNaN(inicio) || isNaN(fin)){

		if(isNaN(inicio)){

			$('.errorI').text('Error, La fecha debe ser númerica');

			$(this).focus();

		}else if(isNaN(fin)){

			$('.errorF').text('Error, La fecha debe ser númerica');

			$(this).focus();
		}

	}else if((inicio<10000000 || inicio>=100000000)&& inicio != ""){

		$('.errorI').text('Error, La fecha no es correcta');

		$(this).focus();

	}else if((fin<10000000 || fin>=100000000) && fin!=""){

		$('.errorF').text('Error, La fecha no es correcta');

		$(this).focus();

	}
	if((fin - inicio < 0) && (inicio != "" && fin != "")) {

        alert("La fecha de inicio es mayor que la de fin");

		$('#F_fin').css('color','red');

		return false;

    }
}

/* me comunico con la API del periodico */

var Listado_noticias = [];

function buscar(busqueda,inicio,fin) { //El script para pedir la información a la API la proporciona la misma API

    Listado_noticias = []; //reinicializamos el array

    var url = "https://api.nytimes.com/svc/search/v2/articlesearch.json";

    url += '?' + $.param({

        'api-key': "0634bb7a344949129822a2dfc632ba7e",

        'q': busqueda,

        'begin_date': inicio,

        'end_date': fin

    });

    $.ajax({ // No le hacemos un parse al resultado porque ya viene en formato de objetos JSON.

        url: url,

        method: 'GET',

    }).done(function(result) {

        for (var i=0; i<result.response.docs.length; i++) {

            Listado_noticias.push([formatearFecha(result.response.docs[i].pub_date), result.response.docs[i].headline.main, result.response.docs[i].web_url]);

        }

        if ($('#cabecera').hasClass('panel-danger')) {

            $('#cabecera').removeClass('panel panel-danger').addClass('panel panel-primary');

        }

        $('#noticias_externas').empty();

        Noticias();

    }).fail(function(jqXHR, textStatus, errorThrown) {

        $('#cabecera').removeClass('panel panel-primary').addClass('panel panel-danger');

        $('#noticias_externas').html('<li> No se han encontrado resultados </li>');

        throw err;
    });
}

function formatearFecha(fecha) {

    var fechaRecu = fecha.split("-");

    fecha = fechaRecu[0]+fechaRecu[1]+fechaRecu[2];

    return parseInt(fecha);

}


function Noticias() {

    var Ordenadas = [];

    switch ($('#ordenar option:checked').val()) {

        case 'antiguas':

            Ordenadas = ordenarNoticias(Listado_noticias, 'ASC');

            break;

        case 'recientes':

            Ordenadas = ordenarNoticias(Listado_noticias, 'DES');

            break;
    }

    $("#noticias_externas").empty();

    for (var i=0; i<Ordenadas.length; i++) {
        $("#noticias_externas").append("<li><strong>Fecha</strong>:" + FechaSalida(Ordenadas[i][0]) +
							"<br> <strong>Noticia</strong>: " + Ordenadas[i][1] +
							"<br> <strong>URL</strong>: <a href="+ Ordenadas[i][2]+">" + Ordenadas[i][2]+"</a>");
    }
}

function FechaSalida(fecha){

	var date = fecha.toString();
	var anyo = date.substring(0,4);
	var mes = date.substring(4,6);
	var dia = date.substring(6,8);
	return dia + "-" + mes + "-" + anyo ;

}

function ordenarNoticias(noticias, orden) {

    if (orden==="ASC") {

        noticias.sort(function (a, b) {

            return a[0]-b[0]; // ordenacion de menor a mayor

        });
    } else if(orden==="DES") {

        noticias.sort(function (a, b) {

            return b[0]-a[0];// ordenacion de mayor a menor

        });

    }

    return noticias;
}
